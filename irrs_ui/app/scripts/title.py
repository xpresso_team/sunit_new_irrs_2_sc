import os
import re
def QuoteForPOSIX(string):
    return "\\'".join("'" + p + "'" for p in string.split("'"))


if __name__ == '__main__':
        for filename in os.listdir('../questions'):
                title =  open("../question_title/"+ filename,"w+")
                question = open("../questions/"+filename,"r").read()
		question = question.replace("\r","")
		question = question.replace("\n","")
		question = QuoteForPOSIX(question)
		title.write(question)
