
#! /bin/bash
## This script is used to setup the build environment. It is used setup the build and test environment. It performs
## either of these tasks
## 1. Install Linux Libraries
## 2. Setup environment
## 3. Download data required to perform build.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user
