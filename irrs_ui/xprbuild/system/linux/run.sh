#! /bin/bash
## This script is used to run the project. It shuold contain the script which will run the project
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user


# Run the application 
cd {ROOT_FOLDER}  
node ${ROOT_FOLDER}/${PROJECT_NAME}/forever.js
